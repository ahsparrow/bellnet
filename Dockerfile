FROM python:3.7-slim

WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY . /app

VOLUME /var/lib/bellnet

ENTRYPOINT ["python"]
