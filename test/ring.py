import argparse
import sys
import time

BELLS = "123456"

parser = argparse.ArgumentParser()
parser.add_argument("device")
parser.add_argument("--delay", type=float, default=0.3)
args = parser.parse_args()

with open(sys.argv[1], "w") as pty:
    while 1:
        for b in BELLS:
            pty.write(b)
            pty.flush()
            time.sleep(args.delay)
