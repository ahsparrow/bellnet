import logging
import os
import os.path
import serial
import time

import gevent
import gevent.queue

# Bagley serial interface band rate
BAUD_RATE = 2400

# End-of-touch timeout
EOT_TIMEOUT = 3

# Output queue size
QUEUE_SIZE = 5

# Number of strikes before recording a log file
MIN_LOG_LENGTH = 100

# Bagley bell symbols
BELLS = b"1234567890ET"

class Bagley:
    def __init__(self, device, delays, log_dir=None):
        self.device = device
        self.delays = delays
        self.log_dir = log_dir
        self.log = []

        self.logger = logging.getLogger("bagley")
        self.logger.info("Delays: %s" % delays)

        self.bells = BELLS[:len(delays)]
        self.queues = []

        self.touch_time = 0

    # Open serial port and spawn greenlet to read Bagley data
    def start(self):
        self._open()
        self.greenlet = gevent.spawn(self._read)

    # Add bell server queue
    def register(self, address):
        q = gevent.queue.Queue(QUEUE_SIZE)
        self.queues.append({'q': q, 'address': address})
        return q

    # Remove bell server queue
    def deregister(self, queue):
        for q in self.queues[:]:
            if q['q'] == queue:
                self.queues.remove(q)

    # Returns current touch duration, 0 if not ringing
    def current_touch_duration(self):
        if self.log:
            tim = time.monotonic() - self.log[0]['time']
        else:
            tim = 0

        return tim

    # Returns previous touch duration
    def previous_touch_duration(self):
        return self.touch_time

    def _open(self):
        self.serial = serial.Serial(self.device, BAUD_RATE, timeout=EOT_TIMEOUT)

    def _close(self):
        self.serial.close()

    def _read(self):
        while True:
            bell = self.serial.read()
            tstamp = time.monotonic()
            if not bell:
                # Touch maybe over so check if log needs to be written
                self._check_log()
                continue

            if bell in self.bells:
                self.logger.debug("Bell %c at %f" % (bell[0], tstamp))

                delay = self.delays[self.bells.index(bell)]
                strike_time = tstamp + delay
                data = {'bell': bell.decode(), 'time': strike_time}
                self._log(data)

                # Send data to bell server
                self._send(data)
            else:
                self.logger.warn("Unrecognised Bagley data: %s" % repr(bell.decode()))

    def _send(self, data):
        for q in self.queues[:]:
            try:
                q['q'].put(data, block=False)
            except gevent.queue.Full:
                self.logger.warn("Removing stalled queue")
                self.queues.remove(q)

    def _log(self, data):
        self.log.append(data)

    def _check_log(self):
        if not self.log:
            return

        if len(self.log) > MIN_LOG_LENGTH and self.log_dir:
            # Calculate start time of touch
            self.touch_time = time.monotonic() - self.log[0]['time']
            start_time = time.time() - self.touch_time
            tim = time.localtime(start_time)

            # Create log directory
            dname = time.strftime("bnet_%y%m%d", tim)
            dname_path = os.path.join(self.log_dir, dname)
            if not os.access(dname_path, os.F_OK):
                try:
                    os.mkdir(dname_path)
                    self.logger.info("Created log directory %s" % dname)
                except:
                    self.logger.error("Failed to create log directory %s" % dname_path)
                    self.log = []
                    return

            fname = time.strftime("%H%M%S.bnet", tim)
            fname_path = os.path.join(self.log_dir, dname, fname)
            try:
                with open(fname_path, "w") as log_file:
                    self.logger.info("Writing log file: %s" % fname_path)
                    for d in self.log:
                        log_file.write("%s,%.3f\n" % (d['bell'], d['time']))
            except:
                self.logger.error("Failed to write log file %s" % fname_path)
        else:
            self.logger.info("Discarding log, length %d" % len(self.log))

        self.log = []
