#!/bin/sh

if [ $# -ne 2 ]
then
    echo "Usage: create.sh log_dir serial_dev"
    exit 2
fi

LOG_DIR=$1
SERIAL_DEV=$2

docker rm bellnet
docker create --name bellnet -p 5000:5000 \
  --volume $SERIAL_DEV:/dev/ttyS0 \
  --volume $LOG_DIR:/var/lib/bellnet bellnet \
  wsgi.py --dir /var/lib/bellnet --delays /var/lib/bellnet/delays.txt /dev/ttyS0
