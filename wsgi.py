from gevent import monkey, pywsgi, signal
monkey.patch_all()

import collections
import copy
import logging
import logging.handlers

from bagley import Bagley
import bellnet

WEB_LOG_FORMAT = "%(asctime)s %(levelname)s %(name)s %(message)s"
LOG_FORMAT = "%(asctime)s %(levelname)s %(name)s %(message)s"

def create_app(device, delays, dir, log_deque):
    bagley = Bagley(device, delays, dir)
    bagley.start()

    syncserver = bellnet.make_syncserver()
    syncserver.start()

    bellserver = bellnet.make_bellserver(bagley)
    bellserver.start()

    return bellnet.create_app(bagley, log_deque)

# Sub-class QueueHandler to use deque
class QueueHandler(logging.handlers.QueueHandler):
    def enqueue(self, record):
        self.queue.append(record)

    # Fix bug in standard library
    def prepare(self, record):
        msg = self.format(record)
        record = copy.copy(record)
        record.message = msg
        record.msg = msg
        record.args = None
        record.exc_info = None
        record.exc_text = None
        return record

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("device",
                        help="serial device for Bagley interface")
    parser.add_argument("--dir", required=True,
                        help="bell log directory")
    parser.add_argument("--delays", type=argparse.FileType('r'), required=True,
                        help="delay configuration file")
    parser.add_argument("--logfile", default="",
                        help="log file, syserr if not specified")
    parser.add_argument("--loglevel", default="INFO",
                        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
                        help="log file logging level")
    args = parser.parse_args()

    # Deque log handler for web interface
    log_deque = collections.deque(maxlen=500)
    deque_handler = QueueHandler(log_deque)
    deque_handler.setLevel(logging.INFO)

    formatter = logging.Formatter(WEB_LOG_FORMAT, datefmt="%H:%M:%S %d %b")
    deque_handler.setFormatter(formatter)

    logging.getLogger().addHandler(deque_handler)

    # Log handler
    if args.logfile:
        handler = logging.handlers.RotatingFileHandler(
                args.logfile, maxBytes=1000000, backupCount=1)
    else:
        # Stderr handler
        handler = logging.StreamHandler()
    handler.setLevel(getattr(logging, args.loglevel))

    formatter = logging.Formatter(LOG_FORMAT)
    handler.setFormatter(formatter)

    logging.getLogger().addHandler(handler)
    logging.getLogger().setLevel(logging.DEBUG)

    # Get sensor deplays
    delays_str = args.delays.readline().strip()
    delays = [float(delay) for delay in delays_str.split(" ")]

    app = create_app(args.device, delays, args.dir, log_deque)

    server = pywsgi.WSGIServer(("0.0.0.0", 5000), app, log=None)

    def stop(server):
        def func():
            server.stop()
        return func

    signal(signal.SIGTERM, stop(server))

    server.serve_forever()
