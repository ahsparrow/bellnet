import json
import logging
import time

from gevent.server import StreamServer

SYNC_PORT = 57654
BELL_PORT = 57655

def synchandler(socket, address):
    logging.info("New sync connection from %s:%s" % address)

    with socket.makefile("r") as rfileobj:
        while True:
            data = rfileobj.readline()
            if not data:
                logging.info("Disconnect sync %s" % address[0])
                break

            try:
                req = json.loads(data)
            except json.JSONDecodeError:
                logging.warning("Non-JSON request from %s" % address[0])
                continue

            if type(req) is not dict:
                logging.warning("Non-dict request from %s" % address[0])
                continue

            req['time'] = time.monotonic()
            jsondata = json.dumps(req).encode()
            try:
                socket.sendall(jsondata)
            except:
                logging.info("Disconnect sync %s" % address[0])
                break

def make_bellhandler(bagley):
    def bellhandler(socket, address):
        logging.info("New bell connection from %s:%s" % address)

        queue = bagley.register(address)

        while True:
            data = queue.get()

            jsondata = json.dumps(data).encode()
            try:
                socket.sendall(jsondata)
            except:
                logging.info("Disconnect bell %s" % address[0])
                break

        bagley.deregister(queue)

    return bellhandler

def make_syncserver():
    server = StreamServer(("0.0.0.0", SYNC_PORT), synchandler)
    return server

def make_bellserver(bagley):
    server = StreamServer(("0.0.0.0", BELL_PORT), make_bellhandler(bagley))
    return server
