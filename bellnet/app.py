from io import BytesIO
from pathlib import Path
from time import strftime, strptime
from zipfile import ZipFile

from flask import Flask, flash, make_response, redirect, render_template, request, url_for

def create_app(bagley, logdeque):
    app = Flask(__name__)

    # Required for message flashing
    app.secret_key = b'anotverysecretkey'

    @app.route("/")
    def logs():
        log_path = Path(bagley.log_dir)

        # Path to each log directory
        day_paths = [p for p in log_path.glob("bnet_*") if p.is_dir()]

        # Dates for each log directory
        dates = [strftime("%a %d-%b-%y", strptime(p.name[-6:], "%y%m%d"))
                 for p in day_paths]

        # Number of bnet files in each log directory
        files = [len(list(p.glob("*.bnet"))) for p in day_paths]

        logs = [{'date': dt, 'dir': path.name, 'files': files}
                for dt, path, files in zip(dates, day_paths, files)]

        return render_template("logfile.html", logs=logs)

    @app.route("/status/")
    def status():
        delays = bagley.delays
        connections = [q['address'] for q in bagley.queues]
        return render_template("status.html",
                delays=delays,
                connections=connections,
                log=reversed(logdeque),
                current_touch=int(bagley.current_touch_duration()),
                previous_touch=int(bagley.previous_touch_duration()))

    @app.route("/download/")
    def download():
        log_dir = request.values['btn']
        app.logger.debug("Download request %s" % log_dir)

        buf = BytesIO()
        zip = ZipFile(buf, mode="w")
        for p in Path(bagley.log_dir, log_dir).glob("*.bnet"):
            zip.write(p, arcname=p.name)

        resp  = make_response(buf.getvalue())
        resp.headers['Content-Type'] = "application/zip"
        resp.headers['Content-Disposition'] = "attachment; filename=%s.zip" % log_dir

        return resp

    @app.route("/delete/", methods=["POST"])
    def delete():
        log_dir = request.values['btn']
        app.logger.debug("Delete request %s" % log_dir)

        path = Path(bagley.log_dir, log_dir)
        try:
            for p in path.glob("*.bnet"):
                p.unlink()

            path.rmdir()
            app.logger.info("Deleted log directory %s" % log_dir)
            flash("Deleted log directory %s" % log_dir, "success")
        except OSError:
            app.logger.warn("Failed to delete log directory %s" % log_dir)
            flash("Failed to delete %s" % log_dir, "danger")

        return redirect(url_for('logs'))

    return app
